import React, { useState, useEffect, useCallback } from "react";
import { FaComments, FaPaintBrush, FaBoxes, FaBullhorn } from "react-icons/fa";
import { IoIosOptions, IoIosTrendingUp, IoIosClose } from "react-icons/io";
import { IconContext } from "react-icons";

// static data
const RESOURCE = [
  {
    title: "Consult",
    icon: <FaComments />,
    description:
      "Co-create, design thinking; strengthen infrastructure resist granular Revolution circular, movements or framework social impact low-hanging fruit. Save the world compelling revolutionary progress."
  },
  {
    title: "Design",
    icon: <FaPaintBrush />,
    description:
      "Policymaker collaborates collective impact humanitarian shared value vocabulary inspire issue outcomes agile. Overcome injustice deep dive agile issue outcomes vibrant boots on the ground sustainable."
  },
  {
    title: "Develop",
    icon: <FaBoxes />,
    description:
      "Revolutionary circular, movements a or impact framework social impact low-hanging. Save the compelling revolutionary inspire progress. Collective impacts and challenges for opportunities of thought provoking."
  },
  {
    title: "Marketing",
    icon: <FaBullhorn />,
    description:
      "Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile, replicable, effective altruism youth. Mobilize commitment to overcome injustice resilient, uplift social transparent effective."
  },
  {
    title: "Manage",
    icon: <IoIosOptions />,
    description:
      "Change-makers innovation or shared unit of analysis. Overcome injustice outcomes strategize vibrant boots on the ground sustainable. Optimism, effective altruism invest optimism corporate social."
  },
  {
    title: "Evolve",
    icon: <IoIosTrendingUp />,
    description:
      "Activate catalyze and impact contextualize humanitarian. Unit of analysis overcome injustice storytelling altruism. Thought leadership mass incarceration. Outcomes big data, fairness, social game-changer."
  }
];

// scrolling handler throttle
let lastScrollY = 0;
let ticking = false;

// 10 minutes delay
const NEWSLETTER_NOTIF_DELAY_STORAGE_KEY = "kautsar|cermatientrytest";
const NEWSLETTER_NOTIF_DELAY = 600000;
const checkNewsletterNotifDelay = () => {
  const delay = window.localStorage.getItem(NEWSLETTER_NOTIF_DELAY_STORAGE_KEY);
  const allow = !delay || parseInt(delay) <= new Date().getTime();
  return allow;
};

function App() {
  const [
    cookiesPolicyNotifClassNames,
    setCookiesPolicyNotifClassNames
  ] = useState(["cookies__policy__notif__container"]);
  const [cookiesPolicyNotifHeight, setCookiesPolicyNotifHeight] = useState(0);
  const cookiesPolicyNotifRef = useCallback(node => {
    if (node !== null) {
      setCookiesPolicyNotifHeight(node.getBoundingClientRect().height);
    }
  }, []);

  const hideCookiesPolicyNotif = () => {
    setCookiesPolicyNotifClassNames(
      cookiesPolicyNotifClassNames.slice(0, 1).concat("notif-hidden")
    );
  };

  const [newsletterNotifClassNames, setNewsletterNotifClassNames] = useState([
    "newsletter__notif__container"
  ]);
  const [newsletterNotifHeight, setNewsletterNotifHeight] = useState(0);
  const newsletterNotifRef = useCallback(node => {
    if (node !== null) {
      setNewsletterNotifHeight(node.getBoundingClientRect().height);
    }
  }, []);

  const hideNewsletterNotif = () => {
    setNewsletterNotifClassNames(
      newsletterNotifClassNames.slice(0, 1).concat("nl-notif-hidden")
    );
    // set next delay
    window.localStorage.setItem(
      NEWSLETTER_NOTIF_DELAY_STORAGE_KEY,
      new Date().getTime() + NEWSLETTER_NOTIF_DELAY
    );
  };

  useEffect(() => {
    const handleScroll = () => {
      lastScrollY = window.scrollY;
      if (
        // 1/3 way scrolling
        lastScrollY >= window.innerHeight / 3 &&
        checkNewsletterNotifDelay()
      ) {
        setNewsletterNotifClassNames(n =>
          n.slice(0, 1).concat("nl-notif-shown")
        );
      }
      if (!ticking) {
        window.requestAnimationFrame(() => {
          ticking = false;
        });
        ticking = true;
      }
    };

    window.addEventListener("scroll", handleScroll, true);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const isCookiesPolicyNotifDisplayed = !cookiesPolicyNotifClassNames.includes(
    "notif-hidden"
  );

  return (
    <div className="root">
      <div
        ref={cookiesPolicyNotifRef}
        className={cookiesPolicyNotifClassNames.join(" ")}
      >
        <div className="content">
          <div className="text">
            By accessing and using this website, you acknowledge that you have
            read and understand our <a href="#">Cookie Policy</a>,{" "}
            <a href="#">Privacy Policy</a>, and our{" "}
            <a href="#">Terms of Service</a>.
          </div>
          <div className="close__button" onClick={hideCookiesPolicyNotif}>
            Got it
          </div>
        </div>
      </div>
      <div className="jumbotron__background">
        <div className="jumbotron">
          <div className="title_block white">
            <div className="title">Hello! I'm Kautsar</div>
            <div className="sub__title">
              Consult, Design, and Develop Websites
            </div>
            <div className="description">
              Have something great in mind? Feel free to contact me.
              <br />
              I'll help you to make it happen.
            </div>
          </div>
          <div className="action__button">LET'S MAKE CONTACT</div>
          <img className="logo" src="/assets/images/y-logo-white.png" />
        </div>
      </div>
      <div className="main">
        <div className="title_block">
          <div className="title">How Can I Help You?</div>
          <p>
            Our work then targeted, best practices outcomes social innovation
            synergy. <br />
            enture philanthropy, revolutionary inclusive policymaker relief.
            User-centered
            <br />
            program areas scale.
          </p>
        </div>
        <div className="container">
          {RESOURCE.map((el, i) => (
            <div key={i} className="content">
              <div className="title">
                {el.title}
                <IconContext.Provider value={{ color: "#999" }}>
                  {el.icon}
                </IconContext.Provider>
              </div>
              <p>{el.description}</p>
            </div>
          ))}
        </div>
      </div>
      <div
        ref={newsletterNotifRef}
        className={newsletterNotifClassNames.join(" ")}
      >
        <div className="newsletter__notif">
          <div className="description">
            <h2>Get latest updates in web technologies</h2>
            <p>
              I write articles related to web technologies, such as design
              trends, development tools, UI/UX case studies and reviews, and
              more. Sign up to my newsletter to get them all.
            </p>
          </div>
          <form className="form">
            <input type="email" placeholder="Email Address" />
            <button type="submit">Count me in!</button>
          </form>
          <div className="close__button" onClick={hideNewsletterNotif}>
            <IoIosClose size="1.5em" color="white" />
          </div>
        </div>
      </div>

      <div className="footer">
        <span>© 2018 Kautsar. All rights reserved</span>
      </div>
      <style jsx>{`
        .newsletter__notif__container {
          position: fixed;
          bottom: -${newsletterNotifHeight}px;
          left: 0;
          z-index: 2;
          max-width: 640px;
        }
        .newsletter__notif {
          position: relative;
          background: #007bc199;
          padding: 1.5em;
        }
        .newsletter__notif form {
          display: grid;
          grid-template-columns: 3fr 1.5fr;
          grid-gap: 0.5em;
        }
        .newsletter__notif form input {
          height: 1.5em;
          padding-left: 0.5em;
        }
        .newsletter__notif form button {
          background: #ff8000;
          border: none;
          border-radius: 4px;
          color: white;
          text-align: center;
          text-decoration: none;
          font-weight: bold;
        }
        .newsletter__notif .description {
          color: white;
        }
        .newsletter__notif .close__button {
          position: absolute;
          top: 10px;
          right: 10px;
        }
        a {
          text-decoration: none;
        }
        .cookies__policy__notif__container {
          display: flex;
          position: fixed;
          top: 0;
          left: 0;
          right: 0;
          z-index: 2;
          background: #e5e5e5;
          display: flex;
          justify-content: center;
          align-items: center;
          padding: 1em 2em;
          border-bottom: 2px solid blue;
        }
        .cookies__policy__notif__container .content {
          max-width: 720px;
          display: flex;
        }
        .cookies__policy__notif__container .content .text {
          flex: 1;
        }
        .cookies__policy__notif__container .content .close__button {
          user-select: none;
          cursor: pointer;
          border-radius: 6px;
          padding: 0.5em 1em;
          background: #007bc1;
          color: white;
          display: flex;
          justify-content: center;
          align-items: center;
        }
        .footer {
          display: flex;
          justify-content: center;
          align-items: center;
          height: 10vh;
          color: white;
          background: #004a75;
        }
        .main {
          padding: 8vh 2em;
          max-width: 100vw;
          background: #e5e5e5;
        }
        .main .container {
          margin-top: 8vh;
          display: grid;
          grid-template-columns: 1fr 1fr 1fr;
          grid-gap: 2em;
        }
        .main .content {
          padding: 1em;
          border: 2px groove white;
        }
        .content .title {
          display: flex;
          justify-content: space-between;
          align-items: center;
          font-size: 1.5em;
        }
        .root {
          transition: 0.2s all linear;
          margin-top: ${isCookiesPolicyNotifDisplayed
            ? cookiesPolicyNotifHeight
            : 0}px;
          font-size: 14px;
          line-height: 1.5;
        }
        .white {
          color: white;
        }

        .title_block {
          text-align: center;
        }
        .title_block .title {
          font-size: 2em;
        }
        .title_block .sub__title {
          font-size: 1.5em;
          font-weight: bolder;
          padding: 0.2em 0;
        }

        .jumbotron__background {
          background: url("/assets/images/work-desk__dustin-lee.jpg");
          background-position: center;
        }
        .jumbotron {
          position: relative;
          z-index: 1;
          background: #004a7599;
          max-width: 100vw;
          height: auto;
          padding: 25vh 2em;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        .jumbotron .logo {
          position: absolute;
          top: 20px;
          left: 20px;
          width: 50px;
          height: auto;
        }
        .jumbotron .action__button {
          margin: 2em 0;
          padding: 0.7em 1.5em;
          border: 3px solid white;
          border-radius: 4px;
          font-weight: bold;
          user-select: none;
          cursor: pointer;
          color: white;
        }
        .jumbotron .action__button:hover {
          color: rgb(0, 123, 193);
          background: white;
        }
        @media only screen and (max-width: 480px) {
          .main .container {
            grid-template-columns: 1fr;
          }
          .newsletter__notif form {
            grid-template-columns: 1fr;
          }
          .newsletter__notif form button {
            height: 2em;
          }
        }
        @media only screen and (max-width: 960px) and (min-width: 480px) {
          .main .container {
            grid-template-columns: 1fr 1fr;
          }
        }
        @media only screen and (max-width: 960px) {
          .cookies__policy__notif__container .content .close__button {
            margin-top: 0.5em;
          }
          .cookies__policy__notif__container .content {
            flex-direction: column;
            align-items: flex-start;
          }
        }
        .notif-shown {
          animation: slidein 0.8s forwards;
        }
        .notif-hidden {
          top: -${cookiesPolicyNotifHeight}px;
          animation: slideout 0.8s forwards;
        }
        @keyframes slidein {
          0% {
            top: -${cookiesPolicyNotifHeight}px;
          }
          100% {
            top: 0px;
          }
        }
        @keyframes slideout {
          0% {
            top: 0;
          }
          100% {
            top: -${cookiesPolicyNotifHeight};
            display: none;
          }
        }

        .nl-notif-shown {
          animation: nl-slidein 0.3s forwards;
        }
        .nl-notif-hidden {
          animation: nl-slideout 0.3s forwards;
        }
        @keyframes nl-slidein {
          0% {
            bottom: -${newsletterNotifHeight}px;
            left: 0;
          }
          100% {
            bottom: 0px;
            left: 0;
          }
        }
        @keyframes nl-slideout {
          0% {
            bottom: 0px;
            left: 0;
          }
          100% {
            bottom: -${newsletterNotifHeight};
            left: 0;
          }
        }
      `}</style>
    </div>
  );
}

export default App;
